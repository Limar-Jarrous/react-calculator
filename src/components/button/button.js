import React from 'react';
import './button.css';

const Button = (props) => {
    
    const styles = {
        gridArea:`${props.id}`,
        background: `${props.color}`,
    }


    return (
        <button className="button-body" 
                style={styles}

                onClick={ (e) => {
                    (props.name === '*' || props.name === '/' || props.name === '+' || props.name === '-') 
                        ? props.handleOperators(props.name, e) 
                        : props.name === '.'
                            ? props.handleDecimal()
                            : props.name === '='
                                ? props.handleEvaluate()
                                : props.name === 'C'
                                    ? props.initialize()
                                    : props.handleNumbers(props.name, e)}
                }>
            {props.name}
        </button>
    );
}
 
export default Button;