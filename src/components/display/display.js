import React from 'react';
import './display.css';


const Display = (props) => {
    return (
        <div className="display-screen" id="display">
            <div className="first-row">
                {props.formula}
            </div>
            <div className="second-row">
                {props.result}
            </div>
        </div>
    );
}
 
export default Display;