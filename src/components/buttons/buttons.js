import React from 'react';
import Button from '../button/button';
import './buttons.css';


const buttonsSet = [
    {id: 'equal', name:'=', color: '#2E86C1'},
    {id: 'clear', name:'C', color: '#D2302E'},
    {id: 'add', name:'+'},       {id: 'subtract', name:'-'},
    {id: 'multiply', name:'*'},  {id: 'divide', name:'/'},
    {id: 'zero', name:'0'},      {id: 'one', name:'1'},
    {id: 'two', name:'2'},       {id: 'three', name:'3'},
    {id: 'four', name:'4'},      {id: 'five', name:'5'},
    {id: 'six', name:'6'},       {id: 'seven', name:'7'},
    {id: 'eight', name:'8'},     {id: 'nine', name:'9'},
    {id: 'decimal', name:'.'},
];


const Buttons = (props) => {

    let btns = buttonsSet.map( btn => (
        <Button key={btn.id} 
                id={btn.id} 
                name={btn.name}
                color={btn.color}
                handleNumbers = {props.handleNumbers}
                handleDecimal = {props.handleDecimal}
                handleEvaluate = {props.handleEvaluate}
                handleOperators = {props.handleOperators}
                initialize = {props.initialize}
                />
    ));

    return (
        <div className="buttons-container">
            {btns}
        </div>
    );
}
 
export default Buttons;